library IEEE;
use IEEE.std_logic_1164.all;  -- Para std_logic
use IEEE.numeric_std.all;     -- Para unsigned

entity Mux_4inputs is	-- MULTIPLEXOR.
   PORT(
		Num1		: IN STD_LOGIC_VECTOR (3 downto 0); -- Primer n�mero.
		Num2		: IN STD_LOGIC_VECTOR (3 downto 0); -- Segundo n�mero.
		Num3		: IN STD_LOGIC_VECTOR (3 downto 0); -- Tercer n�mero.
		Num4		: IN STD_LOGIC_VECTOR (3 downto 0); -- Cuarto n�mero.
		Ctrl		: IN STD_LOGIC_VECTOR (3 downto 0);
		Num_out	: OUT STD_LOGIC_VECTOR (3 downto 0) 
	);
end Mux_4inputs;

architecture behavioral of Mux_4inputs is

begin

	with Ctrl select Num_out <=
		Num1 when "1000",
		Num2 when "0100",
		Num3 when "0010",
		Num4 when "0001",
		"1111" when others;
	
end behavioral;
