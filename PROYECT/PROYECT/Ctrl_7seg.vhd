library IEEE;
use IEEE.std_logic_1164.all;  -- Para std_logic
use IEEE.numeric_std.all;     -- Para unsigned

entity Ctrl_7seg is	-- CONTROLADOR DE 7 SEGMENTOS.
   PORT( 
		Num1			: IN STD_LOGIC_VECTOR (3 downto 0); -- Primer n�mero.
		Num2			: IN STD_LOGIC_VECTOR (3 downto 0); -- Segundo n�mero.
		Num3			: IN STD_LOGIC_VECTOR (3 downto 0); -- Tercer n�mero.
		Num4			: IN STD_LOGIC_VECTOR (3 downto 0); -- Cuarto n�mero.
		Clk			: IN STD_LOGIC; -- Reloj.
		Rst			: IN STD_LOGIC; -- Reset.
		display_out	: OUT STD_LOGIC_VECTOR (6 downto 0);
		Char_out		: OUT STD_LOGIC_VECTOR (3 downto 0) 
	);
end Ctrl_7seg;

architecture dataflow of Ctrl_7seg is

-- COMPONENTES.

	component Decodificador
	PORT(
		code 	: IN  STD_LOGIC_VECTOR (3 downto 0);
		led	: OUT STD_LOGIC_VECTOR (6 downto 0)
	);
	end component;

	component Mux_4inputs
	PORT(
		 Num1: IN STD_LOGIC_VECTOR (3 downto 0);
		 Num2: IN STD_LOGIC_VECTOR (3 downto 0);
		 Num3: IN STD_LOGIC_VECTOR (3 downto 0);
		 Num4: IN STD_LOGIC_VECTOR (3 downto 0);
		 Ctrl: IN STD_LOGIC_VECTOR (3 downto 0);
		 Num_out: OUT STD_LOGIC_VECTOR (3 downto 0)
		);
	end component;

	component ContadorAro
	PORT(
		RST : IN STD_LOGIC;
		CLK : IN STD_LOGIC;
		DAT : OUT STD_LOGIC_VECTOR(3 downto 0)
	);
	end component;

-- SE�ALES.

	signal Sel_Char   : STD_LOGIC_VECTOR (3 downto 0);
	signal Num_Activo : STD_LOGIC_VECTOR (3 downto 0);

begin

-- INSTANCIAS.
		
	Ins_Contador: ContadorAro port map(
		DAT => Sel_Char,
		CLK => Clk,
		RST => Rst
	);
	
	Ins_Mux: Mux_4inputs port map(
		Num1 		=> Num1,
		Num2 		=> Num2,
		Num3 		=> Num3,
		Num4 		=> Num4,
		Ctrl 		=> Sel_Char,
		Num_out 	=> Num_Activo
	);
	
	Ins_Decod: Decodificador port map(
		code	=> Num_Activo,
		led 	=> display_out
	);
	
	Char_out <= Sel_Char;

end dataflow;
