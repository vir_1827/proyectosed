library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Proyecto_final is	-- ENTIDAD.
	generic (
		clk_freq: positive := 50000000
	);
    PORT (
		BOTON1 		: IN STD_LOGIC; -- MODO CUENTA ATR�S: Reset // MODO PROGRAMACI�N: Suma 1 al d�gito seleccionado.
		BOTON2 		: IN STD_LOGIC; -- MODO CUENTA ATR�S: Pausa // MODO PROGRAMACI�N: Resta 1 al d�gito seleccionado.
		BOTON3 		: IN STD_LOGIC; -- MODO PROGRAMACI�N: Cambio de d�gito.
		BOTON4 		: IN STD_LOGIC; -- Cambio de modo.
		clock  		: IN STD_LOGIC; -- Reloj.
		reset  		: IN STD_LOGIC; -- Reset.
		led 	 		: OUT STD_LOGIC_VECTOR (7 downto 0); -- Leds.
		digito_out  : OUT STD_LOGIC_VECTOR (3 downto 0); 
		display_out : OUT STD_LOGIC_VECTOR (7 downto 0) 
	);
end Proyecto_final;

architecture dataflow of Proyecto_final is

-- SE�ALES.

	signal button1				: STD_LOGIC;
	signal button2				: STD_LOGIC;
	signal button3				: STD_LOGIC;
	signal button4				: STD_LOGIC;
	signal clk100				: STD_LOGIC;
	signal RESET_cont			: STD_LOGIC;
	signal M1_out_prog  		: STD_LOGIC_VECTOR(3 DOWNTO 0); 
	signal M0_out_prog   	: STD_LOGIC_VECTOR(3 DOWNTO 0); 
	signal S1_out_prog   	: STD_LOGIC_VECTOR(3 DOWNTO 0);
	signal S0_out_prog   	: STD_LOGIC_VECTOR(3 DOWNTO 0);
	signal digito_out_prog 	: STD_LOGIC_VECTOR(3 DOWNTO 0);
	signal M1_out_cont   	: STD_LOGIC_VECTOR(3 DOWNTO 0); 
	signal M0_out_cont   	: STD_LOGIC_VECTOR(3 DOWNTO 0); 
	signal S1_out_cont   	: STD_LOGIC_VECTOR(3 DOWNTO 0);
	signal S0_out_cont   	: STD_LOGIC_VECTOR(3 DOWNTO 0);
	signal digito_out_cont 	: STD_LOGIC_VECTOR(3 DOWNTO 0);
	signal mm1					: STD_LOGIC_VECTOR(3 downto 0);
	signal mm0					: STD_LOGIC_VECTOR(3 downto 0);
	signal ss1					: STD_LOGIC_VECTOR(3 downto 0);
	signal ss0					: STD_LOGIC_VECTOR(3 downto 0);
	signal char					: STD_LOGIC_VECTOR(3 downto 0);
	signal ESTADO				: STD_LOGIC:= '0';

-- COMPONENTES.

	component modulo_grafico
		PORT(
			Num1			: IN STD_LOGIC_VECTOR (3 downto 0);
			Num2			: IN STD_LOGIC_VECTOR (3 downto 0);
			Num3			: IN STD_LOGIC_VECTOR (3 downto 0);
			Num4			: IN STD_LOGIC_VECTOR (3 downto 0);
			Set_flicker : IN STD_LOGIC_VECTOR (3 downto 0);
			Clk			: IN STD_LOGIC;
			Rst			: IN STD_LOGIC;
			display_out : OUT STD_LOGIC_VECTOR (7 downto 0);
			Char_out		: OUT STD_LOGIC_VECTOR (3 downto 0)
		);
	end component;

	component program_cuenta 
		PORT (     
			Control	  : IN  STD_LOGIC; -- Se�al de activaci�n.
			menos		  : IN  STD_LOGIC; -- Pulsador restar 1 al digito seleccionado.
			mas		  : IN  STD_LOGIC; -- Pulsador sumar 1 al digito seleccionado.
			Digito	  : IN  STD_LOGIC; -- Seleccionar siguiente digito.
			clk		  : IN  STD_LOGIC; -- Reloj.		
			M1_out     : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); -- SALIDA Segundo digito de los minutos.    
			M0_out     : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); -- SALIDA Primer digito de los minutos.     
			S1_out     : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); -- SALIDA Segundo digito de los segundos.    
			S0_out     : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); -- SALIDA Primer digito de los segundos.
			digito_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0) -- Codigo del digito que se esta editando.
		);
	end component;

	component Cuenta_atras
		PORT (
			clk  		  : IN  STD_LOGIC; -- Reloj.
			reset		  : IN  STD_LOGIC; -- Reset.
			pausa		  : IN  STD_LOGIC; -- Pausa.
			M1_in   	  : IN STD_LOGIC_VECTOR(3 DOWNTO 0); -- Primer digito de los minutos.
			M0_in   	  : IN STD_LOGIC_VECTOR(3 DOWNTO 0); -- Segundo digito de los minutos.
			S1_in   	  : IN STD_LOGIC_VECTOR(3 DOWNTO 0); -- Primer digito de los segundos.
			S0_in   	  : IN STD_LOGIC_VECTOR(3 DOWNTO 0); -- Segundo digito de los segundos.
			M1_out     : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); -- Primer digito de los minutos.
			M0_out     : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); -- Segundo digito de los minutos.
			S1_out     : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); -- Primer digito de los segundos.
			S0_out     : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); -- Segundo digito de los segundos.
			digito_out : OUT std_logic_vector (3 downto 0)
		);
	end component;

	component antirebotes
		PORT (
			clk    : in  std_logic;
			rst    : in  std_logic;
			button : in  std_logic;
			salida : out std_logic
		);
	end component;
	
	component prescaler is

		generic (
			fin : positive;
			fout: positive
		);
		PORT (
			entrada: IN  STD_LOGIC;
			reset  : IN  STD_LOGIC;
			salida : OUT STD_LOGIC
		);
	end component;

begin		-- INSTANCIAS.

	Ins_modulo: modulo_grafico port map(
		Num1 			=> mm1,
		Num2 			=> mm0,
		Num3 			=> ss1,
		Num4 			=> ss0,
		Set_flicker => char,
		Rst 			=> reset,
		Clk 			=> clock,
		Char_out 	=> digito_out,
		display_out => display_out
	);
			
	RESET_cont <= (button1 OR ESTADO);

	Ins_contador: cuenta_atras port map(
		clk   		=> clock,
		reset			=> RESET_cont,
		pausa 		=> button2,
		M1_in 		=> M1_out_prog,
		M0_in 		=> M0_out_prog,
		S1_in 		=> S1_out_prog,
		S0_in 		=> S0_out_prog,
		M1_out 		=> M1_out_cont,
		M0_out 		=> M0_out_cont,
		S1_out 		=> S1_out_cont,
		S0_out 		=> S0_out_cont,
		digito_out 	=> digito_out_cont
	);
	
	Ins_program: program_cuenta port map (
		Control 		=> ESTADO,
		menos 		=> button2,
		mas 			=> button1,
		Digito 		=> button3,
		clk 			=> clock,
		M1_out 		=> M1_out_prog,
		M0_out 		=> M0_out_prog,
		S1_out 		=> S1_out_prog,
		S0_out 		=> S0_out_prog,
		digito_out 	=> digito_out_prog
	);
	
	Ins_clk100: prescaler
		generic map (
			fin  => clk_freq,
			fout => 100
		)
		port map(
			entrada => clock,
			reset => reset,
			salida => clk100
	);	
	
	Ins_antirebotes1: antirebotes port map(
		clk 		=> clk100,
		rst 		=> reset,
		button 	=> BOTON1,
		salida 	=> button1
	);

	Ins_antirebotes2: antirebotes port map(
		clk 		=> clk100,
		rst 		=> reset,
		button 	=> BOTON2,
		salida 	=> button2
	);

	Ins_antirebotes3: antirebotes port map(
		clk 		=> clk100,
		rst 		=> reset,
		button 	=> BOTON3,
		salida 	=> button3
	);

	Ins_antirebotes4: antirebotes port map(
		clk 		=> clk100,
		rst 		=> reset,
		button 	=> BOTON4,
		salida 	=> button4
	);
	
	Estado_select: process (button4)
	
	begin
		if rising_edge(button4) then		
			ESTADO <= not ESTADO;
		else
			ESTADO <= ESTADO;
		end if;
		
	end process;
	
	estado_proc: process (Estado) 
	
	begin
		if ESTADO ='1' then
			mm1 <= M1_out_prog;
			mm0 <= M0_out_prog;
			ss1 <= S1_out_prog;
			ss0 <= S0_out_prog;
			char <= digito_out_prog;
			led <="00001111";
		else
			mm1 <= M1_out_cont;
			mm0 <= M0_out_cont;
			ss1 <= S1_out_cont;
			ss0 <= S0_out_cont;
			char <= digito_out_cont;	
			led <="11110000";	
		end if;
		
	end process;
	
end dataflow;