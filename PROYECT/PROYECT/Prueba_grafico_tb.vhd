library IEEE;
use IEEE.std_logic_1164.all;  -- Para std_logic
use IEEE.numeric_std.all;     -- Para signed, unsigned

entity Prueba_grafico_tb is
end    Prueba_grafico_tb;

architecture behavioral of Prueba_grafico_tb is

   -- Component Declaration for the Unit Under Test (UUT)
   component Prueba_grafico
      port( 
	 Set_flicker: IN std_logic_vector (3 downto 0);
	 Clk: IN std_logic;
	 Rst: IN std_logic;
	 display_out: OUT std_logic_vector (7 downto 0);
	 Char_out: OUT std_logic_vector (3 downto 0) );
   end component;

   --Inputs
   signal RST : std_logic := '0';
   signal CLK : std_logic := '0';
   signal Set_flicker : std_logic_vector (3 downto 0) := "1101";
   --Outputs
   signal Char_out : std_logic_vector(3 downto 0);
   signal display_out : std_logic_vector(7 downto 0);
   -- Definicion del periodo del reloj
   constant CLK_period : time := 20 ns;

begin

   -- Instantiate the Unit Under Test (UUT)
   uut: Prueba_grafico port map ( 
	Set_flicker => Set_flicker,
	rst => RST,
	clk => CLK,
	Char_out => Char_out,
	display_out => display_out);

   CLK_process :process
   begin
        CLK <= '0';
        wait for CLK_period/2;
        CLK <= '1';
        wait for CLK_period/2;
   end process;

   main_process: process
   begin              
      RST <= '1';
      wait for 10 ns;  
      RST <= '0';
      wait for 50 ms;
      Set_flicker <= "0101";
      wait;
   end process;

end behavioral;
