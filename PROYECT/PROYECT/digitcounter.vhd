----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:40:50 01/25/2016 
-- Design Name: 
-- Module Name:    digitcounter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity digitcounter is		-- CONTADOR NEGATIVO

   generic (
		max_count: positive := 9
	);
	
	PORT (
		reset	: IN  STD_LOGIC;
		clk  	: IN  STD_LOGIC;
		npause: IN  STD_LOGIC;
		qin  	: IN  STD_LOGIC_VECTOR (3 downto 0);
		q    	: OUT STD_LOGIC_VECTOR (3 downto 0);
		cout 	: OUT STD_LOGIC
	);
end digitcounter;

architecture behavioral of digitcounter is

	signal q_i: unsigned(q'range);
	
begin

	process (reset, clk)

	begin
		if reset = '1' then
			q_i <= unsigned(qin);
			cout <= '0';
		elsif rising_edge(clk) then
			cout <= '0';
			if npause = '1' then
				q_i <= q_i - 1;
				if q_i = 0 then
					q_i <= to_unsigned (max_count,q_i'length);
					cout <= '1';
				end if;
			end if;
		end if;
		
	end process;
	
	q <= std_logic_vector(q_i);

end behavioral;
