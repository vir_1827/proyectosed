library IEEE;
use IEEE.std_logic_1164.all;  -- Para std_logic
use IEEE.numeric_std.all;     -- Para unsigned

entity Generador_Parpadeo is	-- PARPADEO.
   PORT( 
		Char_in	: IN STD_LOGIC_VECTOR (3 downto 0);
		Set_blink: IN STD_LOGIC_VECTOR (3 downto 0);
		clk		: IN STD_LOGIC;
		Char_out	: OUT STD_LOGIC_VECTOR (3 downto 0)
	);
end Generador_Parpadeo;

architecture Behavioral of Generador_Parpadeo is

begin

	process (Char_in)
	
	begin
		if (clk = '1') then
			Char_out <= Char_in AND NOT Set_blink;
		else
			Char_out <= Char_in;
		end if;

	end process;

end behavioral;
