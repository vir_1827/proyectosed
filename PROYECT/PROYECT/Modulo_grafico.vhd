library IEEE;
use IEEE.std_logic_1164.all;  -- Para std_logic
use IEEE.numeric_std.all;     -- Para unsigned

entity Modulo_grafico is	-- M�DULO GR�FICO. Para imprimir los digitos en los displays.
	
	generic (
	  clk_freq: positive := 50000000	
	);
	
   PORT( 
		Num1			: IN STD_LOGIC_VECTOR (3 downto 0); -- Primer n�mero.
		Num2			: IN STD_LOGIC_VECTOR (3 downto 0); -- Segundo n�mero.
		Num3			: IN STD_LOGIC_VECTOR (3 downto 0); -- Tercer n�mero.
		Num4			: IN STD_LOGIC_VECTOR (3 downto 0); -- Cuarto n�mero.
		Set_flicker	: IN STD_LOGIC_VECTOR (3 downto 0); -- Parpadeo de los displays.
		Clk			: IN STD_LOGIC; -- Reloj.
		Rst			: IN STD_LOGIC; -- Reset.
		display_out	: OUT STD_LOGIC_VECTOR (7 downto 0);
		Char_out		: OUT STD_LOGIC_VECTOR (3 downto 0)
	);
end Modulo_grafico;

architecture dataflow of Modulo_grafico is

-- COMPONENTES.

	component Ctrl_7seg
		PORT( 
			Num1			: IN STD_LOGIC_VECTOR (3 downto 0);
			Num2			: IN STD_LOGIC_VECTOR (3 downto 0);
			Num3			: IN STD_LOGIC_VECTOR (3 downto 0);
			Num4			: IN STD_LOGIC_VECTOR (3 downto 0);
			Clk			: IN STD_LOGIC;
			Rst			: IN STD_LOGIC;
			display_out	: OUT STD_LOGIC_VECTOR (6 downto 0);
			Char_out		: OUT STD_LOGIC_VECTOR (3 downto 0)
		);
	end component;

	component Generador_Parpadeo
		PORT( 
			Char_in	: IN STD_LOGIC_VECTOR (3 downto 0);
			Set_blink: IN STD_LOGIC_VECTOR (3 downto 0);
			clk		: IN STD_LOGIC;
			Char_out	: OUT STD_LOGIC_VECTOR (3 downto 0)
		);
	end component;

	component prescaler is

		generic (
			fin : positive;
			fout: positive
		);
		PORT (
			entrada: IN  STD_LOGIC;
			reset  : IN  STD_LOGIC;
			salida : OUT STD_LOGIC
		);
	end component;

-- SE�ALES.

	signal clock_400		: std_logic;
	signal clock_5			: std_logic;
	signal display_1		: std_logic_vector (6 downto 0);
	signal display_2		: std_logic_vector (6 downto 0);
	signal char_1			: std_logic_vector (3 downto 0);
	signal char_out_temp	: std_logic_vector (3 downto 0);

begin

-- INSTANCIAS.

	Ins_clk400: prescaler
		generic map (
			fin  => clk_freq,
			fout => 400
		)
		port map(
			salida => clock_400,
			entrada => Clk,
			reset => Rst
	);

	Ins_clk5: prescaler
		generic map (
			fin  => clk_freq,
			fout => 5
		)
		port map(
			salida => clock_5,
			entrada => Clk,
			reset => Rst
	);

	Ins_ctrl_7seg: Ctrl_7seg port map(
		Num1 			=> Num1,
		Num2 			=> Num2,
		Num3 			=> Num3,
		Num4 			=> Num4,
		rst 			=> rst,
		clk 			=> clock_400,
		display_out => display_1,
		Char_out 	=> char_1
	);
		
	Ins_flicker: Generador_Parpadeo port map(
		Char_in 		=> char_1,
		Set_blink 	=> Set_flicker,
		clk 			=> clock_5,
		Char_out 	=> char_out_temp
	);
	
	Char_out <= not char_out_temp;
	display_out <= display_1 & not (not Char_1(3) and Char_1(2) and not Char_1(1) and not Char_1(0)) ;

end dataflow;
