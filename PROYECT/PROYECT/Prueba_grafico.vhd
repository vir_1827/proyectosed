library IEEE;
use IEEE.std_logic_1164.all;  -- Para std_logic
use IEEE.numeric_std.all;     -- Para unsigned

entity Prueba_grafico is
   port( 
	pausa: in std_logic;
	reset: in std_logic;
--	 Set_flicker: IN std_logic_vector (3 downto 0);
	 Clk: IN std_logic;
	 Rst: IN std_logic;
	 display_out: OUT std_logic_vector (7 downto 0);
	 Char_out: OUT std_logic_vector (3 downto 0) );
end Prueba_grafico;

architecture dataflow of Prueba_grafico is

-- compoentes
component Modulo_grafico
   port( Num1: IN std_logic_vector (3 downto 0);
	 Num2: IN std_logic_vector (3 downto 0);
	 Num3: IN std_logic_vector (3 downto 0);
	 Num4: IN std_logic_vector (3 downto 0);
	 Set_flicker: IN std_logic_vector (3 downto 0);
	 Clk: IN std_logic;
	 Rst: IN std_logic;
	 display_out: OUT std_logic_vector (7 downto 0);
	 Char_out: OUT std_logic_vector (3 downto 0) );
end component;

component digitcounter is
   generic (
		max_count: positive
	);
	port (
		reset: in  std_logic;
		clk  : in  std_logic;
		npause  : in  std_logic;
		qin  : in  std_logic_vector (3 downto 0);
		q    : out std_logic_vector (3 downto 0);
		cout : out std_logic
	);
end component;

-- se�ales temporarles

signal clk1: std_logic;
signal ss_0: std_logic_vector (3 downto 0):= "0000";
signal ss_1: std_logic_vector (3 downto 0):= "0000";
signal mm_0: std_logic_vector (3 downto 0):= "0000";
signal mm_1: std_logic_vector (3 downto 0):= "0000";

signal ss1: std_logic_vector (3 downto 0):= "0000";
signal ss0: std_logic_vector (3 downto 0):= "0011";
signal mm1: std_logic_vector (3 downto 0):= "0000";
signal mm0: std_logic_vector (3 downto 0):= "0010";

signal cdm: std_logic;
signal cum: std_logic;
signal cds: std_logic;
signal cus: std_logic;

signal not_pause: std_logic := '0';
signal temporal : std_logic_vector (3 downto 0) := "1111";

component clk1Hz
	Port (
        	entrada: in  STD_LOGIC;
        	reset  : in  STD_LOGIC;
        	salida : out STD_LOGIC
    		);
end component;

begin

	Ins_Modulo_grafico: Modulo_grafico port map(
		Num1 => mm_1,
		Num2 => mm_0,
		Num3 => ss_1,
		Num4 => ss_0,
		Set_flicker => temporal,
		rst => rst,
		clk => clk,
		display_out => display_out,
		Char_out => char_out
		);
		
		Ins_clk1Hz: clk1Hz port map(
		entrada => clk,
		reset => rst,
		salida => clk1
		);	
		
		

	decenas_minuto: digitcounter
		generic map (
			max_count => 9
		)
		port map (
			reset => reset,
			clk   => cum,
			npause   => not_pause,
			qin	=> mm1,
			q     => mm_1,
			cout  => cdm
		);

	unidades_minuto: digitcounter
		generic map (
			max_count => 9
		)
		port map (
			reset => reset,
			clk   => cds,
			npause   => not_pause,
			qin	=> mm0,
			q     => mm_0,
			cout  => cum
		);

	decenas_segundo: digitcounter
		generic map (
			max_count => 5
		)
		port map (
			reset => reset,
			clk   => cus,
			npause   => not_pause,
			qin	=> ss1,
			q     => ss_1,
			cout  => cds
		);

	unidades_segundo: digitcounter
		generic map (
			max_count => 9
		)
		port map (
			reset => reset,
			clk   => clk1,
			npause   => not_pause,
			qin	=> ss0,
			q     => ss_0,
			cout  => cus
		);

  	pause_register: process (clk, reset, pausa)
	  variable last_pausa: std_logic;
	begin
		if reset = '1' then
			not_pause <= '0';
		elsif rising_edge(clk) then
			if ss_0  = "0000" and ss_1  = "0000" and mm_0  = "0000" and mm_1  = "0000" then
				not_pause <= '0';
			elsif pausa = '1' and last_pausa /= pausa then
				not_pause <= not not_pause;
			end if;
			last_pausa := pausa;
		end if;
	end process;	

	parpadeo: process (not_pause)
	begin
		if (not_pause = '0') then
			temporal <= "1111";
		else
			temporal <= "0000";
		end if;
	end process;
end dataflow;
