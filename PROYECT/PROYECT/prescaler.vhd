library IEEE;
use IEEE.std_logic_1164.all;  -- Para std_logic
use IEEE.numeric_std.all;     -- Para unsigned


entity prescaler is	-- DIVISOR DE FRECUENCIA.

	generic (
		fin : positive;	-- Frecuencia de entrada.
		fout: positive		-- Frecuencia deseada de salida.
	);
	PORT (
		entrada: in  STD_LOGIC;
		reset  : in  STD_LOGIC;
      salida : out STD_LOGIC
	);
end prescaler;
 
architecture Behavioral of prescaler is

	constant semiperiod: positive := fin / (2 * fout) - 1;
	subtype contador_t is integer range 0 to semiperiod;
   signal temporal: STD_LOGIC;
   signal contador: contador_t;
	
begin

    divisor_frecuencia: process (reset, entrada)
	 begin
        if (reset = '1') then
            temporal <= '0';
            contador <= contador_t'high;
        elsif rising_edge(entrada) then
            if (contador = 0) then
                temporal <= NOT(temporal);
                contador <= contador_t'high;
            else
                contador <= contador - 1;
            end if;
        end if;
		  
    end process;
     
    salida <= temporal;
	 
end Behavioral;
