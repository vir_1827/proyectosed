library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Prueba_Prog_Cuenta is
	generic (
	  clk_freq: positive := 50000000
	);
        Port ( BOTON1 : in  STD_LOGIC; -- menos
					BOTON2 : in  STD_LOGIC; --mas
					BOTON3 : in  STD_LOGIC; --cambio de digito
					clock : in  STD_LOGIC;
					reset : in std_logic;
					digito_out : out  STD_LOGIC_VECTOR(3 downto 0);
					display_out : out  STD_LOGIC_VECTOR (7 downto 0)
	   );
end Prueba_Prog_Cuenta;

architecture dataflow of Prueba_Prog_Cuenta is

signal button1: std_logic;
signal button2: std_logic;
signal button3: std_logic;

signal clk100: std_logic;

signal mm1: std_logic_vector(3 downto 0);
signal mm0: std_logic_vector(3 downto 0);
signal ss1: std_logic_vector(3 downto 0);
signal ss0: std_logic_vector(3 downto 0);
signal char: std_logic_vector(3 downto 0);
signal control: std_logic := '1';

component antirebotes
  port (
     clk    : in  std_logic;
     rst    : in  std_logic;
     button : in  std_logic;
     salida : out std_logic
   );
end component;

component modulo_grafico
	generic (
	  clk_freq: positive
	);
	port(	Num1: IN std_logic_vector (3 downto 0);
		Num2: IN std_logic_vector (3 downto 0);
		Num3: IN std_logic_vector (3 downto 0);
		Num4: IN std_logic_vector (3 downto 0);
		Set_flicker: IN std_logic_vector (3 downto 0);
		Clk: IN std_logic;
		Rst: IN std_logic;
		display_out: OUT std_logic_vector (7 downto 0);
		Char_out: OUT std_logic_vector (3 downto 0)
		);
end component;

component program_reloj
	Port ( 	
	Control		: IN  STD_LOGIC; --Se�al de activaci�n.
	menos			: IN  STD_LOGIC; --Pulsador restar 1 al digito seleccionado.
	mas			: IN  STD_LOGIC; --Pulsador sumar 1 al digito seleccionado.
	Digito		: IN  STD_LOGIC; --Seleccionar siguiente digito.
	clk			: IN  STD_LOGIC; --Reloj
	H1_out   	: OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --SALIDA Segundo digito de la hora.    
	H0_out   	: OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --SALIDA Primer digito de la hora.     
	M1_out   	: OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --SALIDA Segundo digito de los minutos.    
	M0_out   	: OUT STD_LOGIC_VECTOR(3 DOWNTO 0);  --SALIDA Primer digito de los minutos.
	digito_out 	: OUT STD_LOGIC_VECTOR(3 DOWNTO 0) --Codigo del digito que se esta editando.
	);
end component;

component clk100Hz
  port (entrada: in  STD_LOGIC;
        reset  : in  STD_LOGIC;
        salida : out STD_LOGIC
   );
end component;

begin
	
	Ins_modulo: modulo_grafico
		generic map (
		clk_freq => clk_freq
		)
		port map(
		Num1 => mm1,
		Num2 => mm0,
		Num3 => ss1,
		Num4 => ss0,
		Set_flicker => char,
		Rst => reset,
		Clk => clock,
		Char_out => digito_out,
		display_out => display_out
		);
		
	Ins_prog: program_reloj port map (
	
		control  => control,
	   menos  	=> button1,	
		mas  		=> button2,
		digito 	=> button3,
		clk 		=> clock,
      H1_out  	=> mm1,
		H0_out  	=> mm0,
		M1_out  	=> ss1,
		M0_out  	=> ss0,
		digito_out  => char
		);
		
	Ins_clk100Hz: clk100Hz port map(
		entrada => clock,
		reset => reset,
		salida => clk100
		);	

	Ins_antirebotes1: antirebotes port map(
		clk => clk100,
		rst => reset,
		button => BOTON1,
		salida => button1
		);

	Ins_antirebotes2: antirebotes port map(
		clk => clk100,
		rst => reset,
		button => BOTON2,
		salida => button2
		);

	Ins_antirebotes3: antirebotes port map(
		clk => clk100,
		rst => reset,
		button => BOTON3,
		salida => button3
		);

	
end dataflow;

