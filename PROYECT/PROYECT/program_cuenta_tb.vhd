library IEEE;
use IEEE.std_logic_1164.all;  -- Para std_logic
use IEEE.numeric_std.all;     -- Para signed, unsigned

entity program_cuenta_tb is
end    program_cuenta_tb;

architecture Behavioral of program_cuenta_tb is
   --Inputs
        signal Control:  STD_LOGIC := '0'; --Se�al de activaci�n.
	signal menos:  STD_LOGIC; --Pulsador restar 1 al digito seleccionado.
	signal mas:  STD_LOGIC; --Pulsador sumar 1 al digito seleccionado.
	signal Digito:  STD_LOGIC; --Seleccionar siguiente digito.
	signal clk: std_logic;
    --Outputs
	signal S1_out   :  STD_LOGIC_VECTOR(3 DOWNTO 0); --SALIDA Segundo digito de la hora.
        signal S0_out   :  STD_LOGIC_VECTOR(3 DOWNTO 0); --SALIDA Primer digito de la hora.
        signal M1_out   :  STD_LOGIC_VECTOR(3 DOWNTO 0); --SALIDA Segundo digito de los minutos.
        signal M0_out   :  STD_LOGIC_VECTOR(3 DOWNTO 0);  --SALIDA Primer digito de los minutos.
	signal digito_out :  STD_LOGIC_VECTOR(3 DOWNTO 0); --Codigo del digito que se esta editando.
	constant entrada_period : time := 20 ns; 
BEGIN

   uut: entity work.program_cuenta PORT MAP (
	clk => clk,
	control => control,
	menos => menos,
	mas => mas,
	digito => digito,
	S1_out => S1_out,
	S0_out => S0_out,
	M1_out => M1_out,
	M0_out => M0_out,
	digito_out => digito_out
        );
		  
	clk_process :process
        begin
        clk <= '0';
        wait for entrada_period /4;
        clk <= '1';
        wait for entrada_period /4;
    end process;

   main_process: process
   begin      
-- puesto todo a cero
	control <= '0';
	digito <='0';
	mas <= '0';
	menos <= '0';
	wait for 10 ns;

-- probamos si con la se�al de control a 0 no lee las entradas
	menos <= '1';
	wait for 10 ns;
	menos <= '0';
	wait for 10 ns;

--activamos se�al de control
	control <= '1';
	wait for 10 ns;

--restamos dos minutos
	menos <= '1';
	wait for 10 ns;
	menos <= '0';
 	wait for 20 ns;
	menos <= '1';
	wait for 10 ns;
	menos <= '0';
 	wait for 20 ns;

--cambiamos el digito y pasamos a editar minutos de 10 en 10
	digito <= '1';
	wait for 10 ns;
	digito <= '0';
 	wait for 20 ns;
--restamos 10 minutos
	menos <= '1';
	wait for 10 ns;
	menos <= '0';
 	wait for 20 ns;
--sumamos 10 minutos
	mas <= '1';
	wait for 10 ns;
	mas <= '0';
 	wait for 20 ns;

--cambiamos el digito y pasamos a editar horas de 1 en 1
	digito <= '1';
	wait for 10 ns;
	digito <= '0';
 	wait for 20 ns;
--sumamos 1 hora 
	mas <= '1';
	wait for 10 ns;
	mas <= '0';
 	wait for 20 ns;
--cambiamos  digito y pasamos a editar de 10 en 10 horas
	digito <= '1';
	wait for 10 ns;
	digito <= '0';
 	wait for 20 ns;
--sumamos 10 horas
	mas <= '1';
	wait for 10 ns;
	mas <= '0';
 	wait for 20 ns;
--restamos 10 horas
	menos <= '1';
	wait for 10 ns;
	menos <= '0';
 	wait for 20 ns;

   end process;

END;
