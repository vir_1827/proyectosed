--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   14:49:10 01/25/2016
-- Design Name:   
-- Module Name:   F:/4UNI/Proyecto SED/PROYECT/digitcounter_tb.vhd
-- Project Name:  PROYECT
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: digitcounter
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
 
entity digitcounter_tb is
end digitcounter_tb;
 
architecture behavior of digitcounter_tb is 
 
    -- Component Declaration for the Unit Under Test (UUT)
    component digitcounter
    port(
         reset : in  std_logic;
         clk : in  std_logic;
         npause : in  std_logic;
         qin : in  std_logic_vector(3 downto 0);
         q : out  std_logic_vector(3 downto 0);
         cout : out  std_logic
        );
    end component;

   subtype nibble is std_logic_vector(3 downto 0);
	type    counter_t is array(3 downto 0) of nibble;
	
   --Inputs
   signal reset : std_logic;
   signal clk : std_logic;
	signal clkv: std_logic_vector(3 downto 0);
   signal npause : std_logic := '1';
   signal qin : counter_t := ("0000", "0001", others => "0000");

 	--Outputs
   signal q : counter_t;
   signal cout : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
  clkv <= (cout(2), cout(1), cout(0), clk);
  
	-- Instantiate the Unit Under Test (UUT)
  counter: for i in 3 downto 0 generate
   uut: digitcounter port map (
          reset => reset,
          clk => clkv(i),
          npause => npause,
          qin => qin(i),
          q => q(i),
          cout => cout(i)
        );
  end generate;

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 
   reset <= '1' after 0.25 * clk_period, '0' after 0.75 * clk_period;
	
   -- Stimulus process
   stim_proc: process
   begin
      wait for 100 * clk_period;
		assert false
			report "[OK]: Fin de la simulacion."
			severity failure;
   end process;
end;
