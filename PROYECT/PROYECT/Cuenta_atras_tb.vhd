LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY Cuenta_atras_tb IS
END Cuenta_atras_tb;
 
ARCHITECTURE behavior OF Cuenta_atras_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Cuenta_atras
	 	generic (
	  clk_freq: positive := 50000000
	 );
    PORT(
		clk  : IN  STD_LOGIC; --Reloj.
		reset: IN  STD_LOGIC; --Reset.
		pausa: IN  STD_LOGIC; --Pausa.
		M1_in   : IN STD_LOGIC_VECTOR(3 DOWNTO 0); --Primer digito de los minutos.
      M0_in   : IN STD_LOGIC_VECTOR(3 DOWNTO 0); --Segundo digito de los minutos.
      S1_in   : IN STD_LOGIC_VECTOR(3 DOWNTO 0); --Primer digito de los segundos.
      S0_in   : IN STD_LOGIC_VECTOR(3 DOWNTO 0);  --Segundo digito de los segundos.
      M1_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --Primer digito de los minutos.
      M0_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --Segundo digito de los minutos.
      S1_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --Primer digito de los segundos.
      S0_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);  --Segundo digito de los segundos.
		digito_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0) --Seleccion parpadeo 
    );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
	signal pausa : std_logic := '0';
	signal M1_in : std_logic_vector(3 downto 0):="0001";
   signal M0_in : std_logic_vector(3 downto 0):="0000";
   signal S1_in : std_logic_vector(3 downto 0):="0001";
   signal S0_in : std_logic_vector(3 downto 0):="0011";

 	--Outputs
   signal M1_out : std_logic_vector(3 downto 0);
   signal M0_out : std_logic_vector(3 downto 0);
   signal S1_out : std_logic_vector(3 downto 0);
   signal S0_out : std_logic_vector(3 downto 0);
	signal digito_out: std_logic_vector(3 downto 0);
	
   -- Clock period definitions
   constant clk_period : time := 20 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Cuenta_atras
	generic map (
	  clk_freq => 10
	)
	PORT MAP (
          clk => clk,
          reset => reset,
			 pausa => pausa,
          M1_in => M1_in,
          M0_in => M0_in,
			 S1_in => S1_in,
			 S0_in => S0_in,
			 M1_out => M1_out,
          M0_out => M0_out,
			 S1_out => S1_out,
			 S0_out => S0_out,
			 digito_out => digito_out
			
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;		
   end process;
 -- hold reset state for 100 ns

 reset <= '1' after 0.25 * clk_period, '0' after 0.75 * clk_period;

   -- Stimulus process
   stim_proc: process
   begin		
      

		wait until reset = '0';
		wait for 10 ns;
		pausa <= '1';
		wait for 55 ns;
		pausa <= '0';
		wait for 100*clk_period;
		pausa <='1';
		wait for 30 ns;
		pausa <='0';
		wait for 100*clk_period;
		pausa <='1';
		wait for 30 ns;
		pausa <='0';
		
		wait for 1000 * clk_period;
		assert false
			report "[OK]: simulation finished."
			severity failure;
	end process;
END;
