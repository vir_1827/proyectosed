library IEEE;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_1164.ALL;

entity program_reloj is
  
  PORT (     
	Control: IN  STD_LOGIC; --Se�al de activaci�n.
	menos: IN  STD_LOGIC; --Pulsador restar 1 al digito seleccionado.
	mas: IN  STD_LOGIC; --Pulsador sumar 1 al digito seleccionado.
	Digito: IN  STD_LOGIC; --Seleccionar siguiente digito.
	clk: IN  STD_LOGIC; --reloj.
	H1_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --SALIDA Segundo digito de la hora.    
	H0_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --SALIDA Primer digito de la hora.     
	M1_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --SALIDA Segundo digito de los minutos.    
	M0_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);  --SALIDA Primer digito de los minutos.
	digito_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0) --Codigo del digito que se esta editando.
	);
end program_reloj;

architecture Behavioral of program_reloj is
   
signal mm1: UNSIGNED(3 downto 0) := "0000" ;   
signal mm0: UNSIGNED(3 downto 0) := "0000";   
signal hh1: UNSIGNED(3 downto 0) := "0000" ;  
signal hh0: UNSIGNED(3 downto 0) := "0000";   
signal DIG: STD_LOGIC_VECTOR (3 downto 0):= "0001";
signal digito_anterior: STD_LOGIC;
signal menos_anterior: STD_LOGIC;
signal mas_anterior: STD_LOGIC;

begin

principal: process (clk, control, mas, menos, digito)

begin
   if rising_edge (clk) then 
	
		if control = '0' then     
		DIG <= "0001";

		elsif (digito /= digito_anterior and digito = '1') then
			DIG <= to_stdlogicvector(to_bitvector(DIG) rol 1);

		elsif (menos /= menos_anterior and menos = '1') then

			if DIG = "0001" then --Se edita el primer digito de los minutos
				mm0 <= mm0 - 1;
				if mm0 = 0 then
					mm0 <= "1001";
				end if;
			elsif DIG = "0010" then --Se edita el segundo digito de los minutos
				mm1 <= mm1 - 1;  
				if mm1 = 0 then
					mm1 <= "0101";
				end if;
			elsif DIG = "0100" then --Se edita el primer digito de las horas
				hh0 <= hh0 - 1;
				if hh0 = 0 and hh1 < 2 then
					hh0 <= "1001";
				elsif hh0 = 0 and hh1 = 2 then
					hh0 <= "0011";
				end if;
			elsif DIG = "1000" then --Se edita el segundo digito de las horas
				hh1 <= hh1 - 1;
				if hh1 = 0 and hh0 > 3 then
					hh1 <= "0001";
				elsif hh1 = 0 and hh0 < 4 then
					hh1 <= "0010";
				end if;
			end if;

		elsif (mas /= mas_anterior and mas = '1') then

			if DIG = "0001" then --Se edita el primer digito de los minutos
				mm0 <= mm0 + 1;
				if mm0 = 9 then
					mm0 <= "0000";
				end if;
			elsif DIG = "0010" then --Se edita el segundo digito de los minutos
				mm1 <= mm1 + 1;
				if mm1 = 5 then
					mm1 <= "0000";
				end if;
			elsif DIG = "0100" then --Se edita el primer digito de las horas
				hh0 <= hh0 + 1;
				if hh0 = 9 and hh1 < 2 then
					hh0 <= "0000";
				elsif hh0 = 3 and hh1 = 2 then
					hh0 <= "0000";
				end if;
			elsif DIG = "1000" then --Se edita el segundo digito de las horas
				hh1 <= hh1 + 1;
				if hh1 = 1 and hh0 > 3 then
					hh1 <= "0000";
				elsif hh1 = 2 and hh0 < 4 then
					hh1 <= "0000";
				end if;
			end if;
		end if;
		digito_anterior <= digito;
		menos_anterior <= menos;
		mas_anterior <= mas;
	end if;
end process;

--Asignaci�n de se�ales.

    H1_out <= STD_LOGIC_VECTOR(hh1);
    H0_out <= STD_LOGIC_VECTOR(hh0);
    M1_out <= STD_LOGIC_VECTOR(mm1);
    M0_out <= STD_LOGIC_VECTOR(mm0);

    digito_out <= DIG;

end Behavioral;



