library IEEE;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_1164.ALL;
 
entity Cuenta_atras is	-- CUENTA ATR�S.

	generic (
	  clk_freq: positive := 50000000
	);
	
   PORT (
		clk  		 : IN STD_LOGIC; -- Reloj.
		reset		 : IN STD_LOGIC; -- Reset.	
		pausa		 : IN STD_LOGIC; -- Pausa.
		M1_in   	 : IN STD_LOGIC_VECTOR(3 DOWNTO 0); -- Primer digito de los minutos.
		M0_in   	 : IN STD_LOGIC_VECTOR(3 DOWNTO 0); -- Segundo digito de los minutos.
		S1_in   	 : IN STD_LOGIC_VECTOR(3 DOWNTO 0); -- Primer digito de los segundos.
		S0_in   	 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);  -- Segundo digito de los segundos.
		M1_out    : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); -- Primer digito de los minutos.
		M0_out    : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); -- Segundo digito de los minutos.
		S1_out  	 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); -- Primer digito de los segundos.
		S0_out    : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);  -- Segundo digito de los segundos.
		digito_out: OUT STD_LOGIC_VECTOR(3 DOWNTO 0) -- Seleccion parpadeo
		  
    );
end Cuenta_atras;
 
architecture dataflow of cuenta_atras is

-- COMPONENTES.

	component digitcounter is
		generic (
			max_count: positive
		);
		PORT (
			reset : IN  STD_LOGIC;
			clk  	: IN  STD_LOGIC;
			npause: IN  STD_LOGIC;
			qin   : IN  STD_LOGIC_VECTOR (3 downto 0);
			q     : OUT STD_LOGIC_VECTOR (3 downto 0);
			cout  : OUT STD_LOGIC
		);
	end component;
	
	component prescaler is

		generic (
			fin : positive;
			fout: positive
		);
		PORT (
			entrada: IN  STD_LOGIC;
			reset  : IN  STD_LOGIC;
			salida : OUT STD_LOGIC
		);
	end component;

-- SE�ALES.

	signal clk1		 : STD_LOGIC;
	signal ss_0		 : STD_LOGIC_VECTOR (3 downto 0):= "0000";
	signal ss_1		 : STD_LOGIC_VECTOR (3 downto 0):= "0000";
	signal mm_0		 : STD_LOGIC_VECTOR (3 downto 0):= "0000";
	signal mm_1		 : STD_LOGIC_VECTOR (3 downto 0):= "0000";
	signal cdm		 : STD_LOGIC;
	signal cum		 : STD_LOGIC;
	signal cds		 : STD_LOGIC;
	signal cus		 : STD_LOGIC;
	signal not_pause: STD_LOGIC := '0';
	signal temporal : STD_LOGIC_VECTOR (3 downto 0) := "1111";

begin
		
	Ins_clk1: prescaler
		generic map (
			fin  => clk_freq,
			fout => 1
		)
		port map(
			entrada => clk,
			reset => reset,
			salida => clk1
	);			

	decenas_minuto: digitcounter
		generic map (
			max_count => 9
		)
		port map (
			reset  => reset,
			clk    => cum,
			npause => not_pause,
			qin	 => M1_in,
			q      => mm_1,
			cout   => cdm
	);

	unidades_minuto: digitcounter
		generic map (
			max_count => 9
		)
		port map (
			reset  => reset,
			clk    => cds,
			npause => not_pause,
			qin	 => M0_in,
			q      => mm_0,
			cout   => cum
	);

	decenas_segundo: digitcounter
		generic map (
			max_count => 5
		)
		port map (
			reset  => reset,
			clk    => cus,
			npause => not_pause,
			qin	 => S1_in,
			q      => ss_1,
			cout   => cds
	);

	unidades_segundo: digitcounter
		generic map (
			max_count => 9
		)
		port map (
			reset  => reset,
			clk    => clk1,
			npause => not_pause,
			qin	 => S0_in,
			q      => ss_0,
			cout   => cus
	);

  	pause_register: process (clk, reset, pausa)
	
		variable last_pausa: STD_LOGIC;
	
	begin
		if reset = '1' then
			not_pause <= '0';
		elsif rising_edge(clk) then
			if ss_0  = "0000" and ss_1  = "0000" and mm_0  = "0000" and mm_1  = "0000" then
				not_pause <= '0';
			elsif pausa = '1' and last_pausa /= pausa then
				not_pause <= not not_pause;
			end if;
			last_pausa := pausa;
		end if;
		
	end process;	

	parpadeo: process (not_pause)
	
	begin
		if (not_pause = '0') then
			temporal <= "1111";
		else
			temporal <= "0000";
		end if;
		
	end process;
	
--ASIGNACION SE�ALES
	
	M1_out <= mm_1;
	M0_out <= mm_0;
	S1_out <= ss_1;
	S0_out <= ss_0;
	digito_out <= temporal;
	
end dataflow;
