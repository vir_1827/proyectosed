
# PlanAhead Launch Script for Post-Synthesis pin planning, created by Project Navigator

create_project -name PROYECT -dir "F:/4UNI/Proyecto SED/PROYECT/planAhead_run_1" -part xc3s200ft256-5
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "F:/4UNI/Proyecto SED/PROYECT/Proyecto_final.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {F:/4UNI/Proyecto SED/PROYECT} }
set_param project.pinAheadLayout  yes
set_property target_constrs_file "Proyecto_final.ucf" [current_fileset -constrset]
add_files [list {Proyecto_final.ucf}] -fileset [get_property constrset [current_run]]
link_design
