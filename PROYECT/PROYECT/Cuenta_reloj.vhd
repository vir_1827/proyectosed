library IEEE;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_1164.ALL;
 
entity contador_reloj is
    PORT (
        clk  : IN  STD_LOGIC; --Reloj de 1Hz.
        reset: IN  STD_LOGIC; --Se�al de reset.
        H1_in   : IN STD_LOGIC_VECTOR(3 DOWNTO 0); --VALOR INICIAL Segundo digito de la hora.
        H0_in   : IN STD_LOGIC_VECTOR(3 DOWNTO 0); --VALOR INICIAL Primer digito de la hora.
        M1_in   : IN STD_LOGIC_VECTOR(3 DOWNTO 0); --VALOR INICIAL Segundo digito de los minutos.
        M0_in   : IN STD_LOGIC_VECTOR(3 DOWNTO 0);  --VALOR INICIAL Primer digito de los minutos.
        H1_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --SALIDA Segundo digito de la hora.
        H0_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --SALIDA Primer digito de la hora.
        M1_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --SALIDA Segundo digito de los minutos.
        M0_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)  --SALIDA Primer digito de los minutos.
    );
end contador_reloj;
 
architecture Behavioral of contador_reloj is
    signal mm1: UNSIGNED(3 downto 0) := "0000" ;
    signal mm0: UNSIGNED(3 downto 0) := "0000";
    signal hh1: UNSIGNED(3 downto 0) := "0000" ;
    signal hh0: UNSIGNED(3 downto 0) := "0000";
begin
    reloj: process (clk, reset) begin
        if reset = '1' then
            hh1 <= UNSIGNED(H1_in);
            hh0 <= UNSIGNED(H0_in);
            mm1 <= UNSIGNED(M1_in);
            mm0 <= UNSIGNED(M0_in);
        elsif rising_edge(clk) then
            mm0 <= mm0 + 1;
            if mm0 = 9 then
                mm1 <= mm1 + 1;
                mm0 <= "0000";
            end if;
            -- Al pasar 59 minutos, contar una hora.
            if mm1 = 5 AND mm0 = 9 then
                hh0 <= hh0 + 1;
                mm1 <= "0000";
            end if;
            if hh0 = 9 then
                hh1 <= hh1 + 1;
                hh0 <= "0000";
            end if;
            -- Al pasar 23:59, regresar a 00:00.
            if hh1 = 2 AND hh0 = 3 AND mm1 = 5 AND mm0 = 9 then
                hh1 <= "0000";
                hh0 <= "0000";
            end if;
        end if;
    end process;
     
    --Asignaci�n de se�ales.
    H1_out <= STD_LOGIC_VECTOR(hh1);
    H0_out <= STD_LOGIC_VECTOR(hh0);
    M1_out <= STD_LOGIC_VECTOR(mm1);
    M0_out <= STD_LOGIC_VECTOR(mm0);
	 
end Behavioral;

