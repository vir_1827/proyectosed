library IEEE;
use IEEE.std_logic_1164.all;  -- Para std_logic
use IEEE.numeric_std.all;     -- Para unsigned

entity ContadorAro is
port ( 
        DAT : out std_logic_vector(3 downto 0);
        RST : in std_logic;
        CLK : in std_logic
        );
end ContadorAro;

architecture Behavioral of ContadorAro is

signal temp : std_logic_vector(3 downto 0):=('1', others => '0');

begin

DAT <= temp;

process(CLK)
begin
    if( rising_edge(CLK) ) then
        if (RST = '1') then
            temp <= ('1', others => '0');
        else
            temp <= to_stdlogicvector(to_bitvector(temp) ror 1);
        end if;
    end if;
end process;
    
end Behavioral;
