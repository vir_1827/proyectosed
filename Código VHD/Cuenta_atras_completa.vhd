library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Cuenta_atras_completa is
    Port ( BOTON1 : in  STD_LOGIC; -- Reset del contador // Restar 1 en programacion
           BOTON2 : in  STD_LOGIC; -- Pausa del contador // Sumar 1 en programacion
           BOTON3 : in  STD_LOGIC; -- Cambio de digito en programacion
	   BOTON4 : in STD_LOGIC; -- Paso de programacion a contador
           CLK : in  STD_LOGIC;
	   digito_out : out std_logic_vector (3 downto 0);
	   M1_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
	   M0_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); 
	   S1_out   : OUT STD_LOGIC_VECTOR(2 DOWNTO 0); 
	   S0_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0) 
	);
end Cuenta_atras_completa;

architecture Behavioral of Cuenta_atras_completa is
	signal ESTADO		: STD_LOGIC := '0';
	signal CLOCK1HZ		: std_logic;
	signal RESET_cont	: STD_LOGIC;
	signal M1_out_prog  	:  STD_LOGIC_VECTOR(3 DOWNTO 0); 
	signal M0_out_prog   	:  STD_LOGIC_VECTOR(3 DOWNTO 0); 
	signal S1_out_prog   	:  STD_LOGIC_VECTOR(2 DOWNTO 0);
	signal S0_out_prog   	:  STD_LOGIC_VECTOR(3 DOWNTO 0);
	signal digito_out_prog 	:  STD_LOGIC_VECTOR(3 DOWNTO 0);
	signal M1_out_cont   	:  STD_LOGIC_VECTOR(3 DOWNTO 0); 
	signal M0_out_cont   	:  STD_LOGIC_VECTOR(3 DOWNTO 0); 
	signal S1_out_cont   	:  STD_LOGIC_VECTOR(2 DOWNTO 0);
	signal S0_out_cont   	:  STD_LOGIC_VECTOR(3 DOWNTO 0);
	signal digito_out_cont 	: std_logic_vector (3 downto 0);
	signal boton4_anterior 	: std_logic;
	
component program_cuenta 
	PORT (     
		Control: IN  STD_LOGIC; --Se�al de activaci�n.
		menos: IN  STD_LOGIC; --Pulsador restar 1 al digito seleccionado.
		mas: IN  STD_LOGIC; --Pulsador sumar 1 al digito seleccionado.
		Digito: IN  STD_LOGIC; --Seleccionar siguiente digito.
		M1_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --SALIDA Segundo digito de la hora.    
		M0_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --SALIDA Primer digito de la hora.     
		S1_out   : OUT STD_LOGIC_VECTOR(2 DOWNTO 0); --SALIDA Segundo digito de los minutos.    
		S0_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);  --SALIDA Primer digito de los minutos.
		digito_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0) --Codigo del digito que se esta editando.
		);
end component;
component Cuenta_atras
	PORT (
        clk  : IN  STD_LOGIC; --Reloj.
        reset: IN  STD_LOGIC; --Reset.
	pausa: IN  STD_LOGIC; --Pausa.
	M1_in   : IN STD_LOGIC_VECTOR(3 DOWNTO 0); --Primer digito de los minutos.
        M0_in   : IN STD_LOGIC_VECTOR(3 DOWNTO 0); --Segundo digito de los minutos.
        S1_in   : IN STD_LOGIC_VECTOR(2 DOWNTO 0); --Primer digito de los segundos.
        S0_in   : IN STD_LOGIC_VECTOR(3 DOWNTO 0);  --Segundo digito de los segundos.
        M1_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --Primer digito de los minutos.
        M0_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --Segundo digito de los minutos.
        S1_out   : OUT STD_LOGIC_VECTOR(2 DOWNTO 0); --Primer digito de los segundos.
        S0_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);  --Segundo digito de los segundos.
		  digito_out : OUT std_logic_vector (3 downto 0)
	);
end component;

component clk1Hz
	Port (
        entrada: in  STD_LOGIC;
        reset  : in  STD_LOGIC;
        salida : out STD_LOGIC
    );
end component;

begin
	RESET_cont <= BOTON1 OR ESTADO;
	
	ESTADO_proc: process (BOTON4)
	begin
		if (boton4_anterior /= BOTON4) then
			ESTADO <= NOT ESTADO;
		end if;
		boton4_anterior <= boton4;
	end process;
	
		Ins_clk1hz: clk1Hz port map(
			entrada => CLK,
			reset => BOTON1,
			salida => CLOCK1HZ
			);
		Ins_contador: cuenta_atras port map(
			clk   => CLOCK1HZ,
			reset	=> RESET_cont,
			pausa => BOTON2,
			M1_in => M1_out_prog,
			M0_in => M0_out_prog,
			S1_in => S1_out_prog,
			S0_in => S0_out_prog,
			M1_out => M1_out_cont,
			M0_out => M0_out_cont,
			S1_out => S1_out_cont,
			S0_out => S0_out_cont,
			digito_out => digito_out_cont
			);
		Ins_program: program_cuenta port map (
			Control => ESTADO,
			menos => BOTON1,
			mas => BOTON2,
			Digito => BOTON3,
			M1_out => M1_out_prog,
			M0_out => M0_out_prog,
			S1_out => S1_out_prog,
			S0_out => S0_out_prog,
			digito_out => digito_out_prog
			);
			
salida: process (digito_out_prog, digito_out_cont,M1_out_prog, M0_out_prog,S1_out_prog, S0_out_prog,
			 M1_out_cont, M0_out_cont, S1_out_cont, S0_out_cont)
		begin
		if (ESTADO = '1') then
			digito_out <= digito_out_prog;
			M1_out <= M1_out_prog;
			M0_out <= M0_out_prog;
			S1_out <= S1_out_prog;
			S0_out <= S0_out_prog;
		else
			digito_out <= digito_out_cont;
			M1_out <= M1_out_cont;
			M0_out <= M0_out_cont;
			S1_out <= S1_out_cont;
			S0_out <= S0_out_cont;
		end if;
end process;
			
end Behavioral;