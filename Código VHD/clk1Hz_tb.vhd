library IEEE;
use IEEE.std_logic_1164.all;  -- Para std_logic
use IEEE.numeric_std.all;     -- Para signed, unsigned

ENTITY clk1Hz_tb IS
END clk1Hz_tb;

ARCHITECTURE behavior OF clk1Hz_tb IS
    COMPONENT clk1Hz
 
    PORT(
        entrada : IN  std_logic;
        reset   : IN  std_logic;
        salida  : OUT std_logic
    );
	END COMPONENT;
 
    -- Entradas
    signal entrada : std_logic := '0';
	 signal reset   : std_logic := '0';
    -- Salidas
    signal salida  : std_logic;
	 
    constant entrada_period : time := 20 ns; 
BEGIN
    -- Instancia de la unidad bajo prueba.
    uut: clk1Hz PORT MAP (
        entrada => entrada,
        reset   => reset,
        salida  => salida
    );
    -- Definición del reloj.
    clk_process :process
			begin
				entrada <= '1';
				wait for entrada_period / 2;
				entrada <= '0';
				wait for entrada_period / 2;
							
    end process;
 
    -- Procesamiento de estímulos.
    rst_process: process
    begin
        reset <= '1'; -- Condiciones iniciales.
        wait for 10 ms;
        reset <= '0'; 
        wait;
    end process;
END;
