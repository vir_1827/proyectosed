library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Prueba_Prog_Cuenta is
    Port ( BOTON1 : in  STD_LOGIC; -- menos
           BOTON2 : in  STD_LOGIC; --mas
           BOTON3 : in  STD_LOGIC; --cambio de digito
           clock : in  STD_LOGIC;
	   reset : in std_logic;
           digito_out : out  STD_LOGIC_VECTOR(3 downto 0);
           display_out : out  STD_LOGIC_VECTOR (7 downto 0)
	   );
end Prueba_Prog_Cuenta;

architecture Behavioral of Prueba_Prog_Cuenta is

signal mm1: std_logic_vector(3 downto 0);
signal mm0: std_logic_vector(3 downto 0);
signal ss1: std_logic_vector(2 downto 0);
signal ss1_fix: std_logic_vector(3 downto 0);
signal ss0: std_logic_vector(3 downto 0);
signal char: std_logic_vector(3 downto 0);
signal control: std_logic := '1';

component modulo_grafico
	port(	Num1: IN std_logic_vector (3 downto 0);
		Num2: IN std_logic_vector (3 downto 0);
		Num3: IN std_logic_vector (3 downto 0);
		Num4: IN std_logic_vector (3 downto 0);
		Set_flicker: IN std_logic_vector (3 downto 0);
		Clk: IN std_logic;
		Rst: IN std_logic;
		display_out: OUT std_logic_vector (7 downto 0);
		Char_out: OUT std_logic_vector (3 downto 0)
		);
end component;
component program_cuenta
	Port ( 	
	Control: IN  STD_LOGIC; --Se�al de activaci�n.
	menos: IN  STD_LOGIC; --Pulsador restar 1 al digito seleccionado.
	mas: IN  STD_LOGIC; --Pulsador sumar 1 al digito seleccionado.
	Digito: IN  STD_LOGIC; --Seleccionar siguiente digito.
	M1_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --SALIDA Segundo digito de la hora.    
	M0_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --SALIDA Primer digito de la hora.     
	S1_out   : OUT STD_LOGIC_VECTOR(2 DOWNTO 0); --SALIDA Segundo digito de los minutos.    
	S0_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);  --SALIDA Primer digito de los minutos.
	digito_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0) --Codigo del digito que se esta editando.
	);
end component;

begin
	ss1_fix <= '0'& ss1;
	
	Ins_modulo: modulo_grafico port map(
		Num1 => mm1,
		Num2 => mm0,
		Num3 => ss1_fix ,
		Num4 => ss0,
		Set_flicker => char,
		Rst => reset,
		Clk => clock,
		Char_out => digito_out,
		display_out => display_out
		);
	Ins_prog: program_cuenta port map (
		control  => control,
	        menos  => BOTON1,	
		mas  => BOTON2,
		digito  => BOTON3,
        	M1_out  => mm1,
        	M0_out  => mm0,
        	S1_out  => ss1,
        	S0_out  => ss0,
		digito_out  => char
		);
end Behavioral;