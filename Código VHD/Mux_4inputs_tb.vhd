library IEEE;
use IEEE.std_logic_1164.all;  -- Para std_logic
use IEEE.numeric_std.all;     -- Para signed, unsigned

entity Mux_4inputs_tb is
end    Mux_4inputs_tb;

architecture Testbench of Mux_4inputs_tb is

   -- Component Declaration for the Unit Under Test (UUT)
   component Mux_4inputs
      port(Num1: IN std_logic_vector (3 downto 0);
	 Num2: IN std_logic_vector (3 downto 0);
	 Num3: IN std_logic_vector (3 downto 0);
	 Num4: IN std_logic_vector (3 downto 0);
	 Ctrl: IN std_logic_vector (3 downto 0);
	 Num_out: OUT std_logic_vector (3 downto 0) );
   end component;

   -- Inputs/Outputs
	signal Num1: std_logic_vector (3 downto 0):= "0001";
	signal Num2: std_logic_vector (3 downto 0):= "0010";
	signal Num3: std_logic_vector (3 downto 0):= "0011";
	signal Num4: std_logic_vector (3 downto 0):= "0100";
	signal Ctrl: std_logic_vector (3 downto 0):= "1000";
	signal Num_out: std_logic_vector (3 downto 0);

begin

   -- Instantiate the Unit Under Test (UUT)
   uut: Mux_4inputs port map (
	Num1 => Num1,
	Num2 => Num2,
	Num3 => Num3,
	Num4 => Num4,
	Ctrl => Ctrl,
	Num_out => Num_out );

-- Stimulus process
stim_proc: process
begin
   -- hold reset state for 100 ns
   wait for 100 ns;
   ---------------------------------------------------------
   Ctrl <= "0100";
   ---------------------------------------------------------
   wait for 100 ns;
   ---------------------------------------------------------
   Ctrl <= "0010";
   ---------------------------------------------------------
   wait for 100 ns;
   ---------------------------------------------------------
   Ctrl <= "0001";
   ---------------------------------------------------------
   wait for 100 ns;
   ---------------------------------------------------------
   Ctrl <= "1000";
   ---------------------------------------------------------
   wait;

end process;

end Testbench;