library IEEE;
use IEEE.std_logic_1164.all;  -- Para std_logic
use IEEE.numeric_std.all;     -- Para unsigned

entity Prueb_antirrebotes is
	port( 
		Boton 	: IN std_logic;
		clk 	: IN std_logic;
		rst 	: IN std_logic;
		led	: Out std_logic );
end Prueb_antirrebotes;

architecture Behavioral of Prueb_antirrebotes is

component antirebotes
  port (
     clk    : in  std_logic;
     rst    : in  std_logic;
     button : in  std_logic;
     salida : out std_logic
   );
end component;

component clk100Hz
  port (entrada: in  STD_LOGIC;
        reset  : in  STD_LOGIC;
        salida : out STD_LOGIC
   );
end component;

signal clk100		: std_logic;
signal boton_temporal 	: std_logic;
signal salida_temp		: std_logic := '0';

begin
	Ins_clk100Hz: clk100Hz port map(
		entrada => clk,
		reset => rst,
		salida => clk100
		);		
	Ins_antirebotes: antirebotes port map(
		clk => clk100,
		rst => rst,
		button => boton,
		salida => boton_temporal
		);

	process (boton_temporal)
	begin
		if rising_edge(boton_temporal) then
			salida_temp <= not salida_temp;
		else
			salida_temp <= salida_temp;
		end if;
	end process;

	led <= salida_temp;
		
end Behavioral;
