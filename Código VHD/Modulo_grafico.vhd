library IEEE;
use IEEE.std_logic_1164.all;  -- Para std_logic
use IEEE.numeric_std.all;     -- Para unsigned

entity Modulo_grafico is
   port( Num1: IN std_logic_vector (3 downto 0);
	 Num2: IN std_logic_vector (3 downto 0);
	 Num3: IN std_logic_vector (3 downto 0);
	 Num4: IN std_logic_vector (3 downto 0);
	 Set_flicker: IN std_logic_vector (3 downto 0);
	 Clk: IN std_logic;
	 Rst: IN std_logic;
	 display_out: OUT std_logic_vector (7 downto 0);
	 Char_out: OUT std_logic_vector (3 downto 0) );
end Modulo_grafico;

architecture dataflow of Modulo_grafico is

-- compoentes
component Ctrl_7seg
   port( Num1: IN std_logic_vector (3 downto 0);
	 Num2: IN std_logic_vector (3 downto 0);
	 Num3: IN std_logic_vector (3 downto 0);
	 Num4: IN std_logic_vector (3 downto 0);
	 Clk: IN std_logic;
	 Rst: IN std_logic;
	 display_out: OUT std_logic_vector (6 downto 0);
	 Char_out: OUT std_logic_vector (3 downto 0) );
end component;

component Generador_Parpadeo
   port( 
	 Char_in: IN STD_LOGIC_VECTOR (3 downto 0);
	 Set_blink: IN STD_LOGIC_VECTOR (3 downto 0);
	 clk: IN STD_LOGIC;
	 Char_out: OUT STD_LOGIC_VECTOR (3 downto 0));
end component;

component clk20Hz
    Port (
        entrada: in  STD_LOGIC;
        reset  : in  STD_LOGIC;
        salida : out STD_LOGIC
    );
end component;

component clk400Hz
    Port (
        entrada: in  STD_LOGIC;
        reset  : in  STD_LOGIC;
        salida : out STD_LOGIC
    );
end component;

-- se�ales temporarles
signal clock_400: std_logic;
signal clock_20: std_logic;
signal display_1: std_logic_vector (6 downto 0);
signal display_2: std_logic_vector (6 downto 0);
signal char_1: std_logic_vector (3 downto 0);
signal char_out_temp: std_logic_vector (3 downto 0);

begin

	Ins_clk400: clk400Hz port map(
		salida => clock_400,
		entrada => Clk,
		reset => Rst
		);
	Ins_clk20: clk20Hz port map(
		salida => clock_20,
		entrada => Clk,
		reset => Rst
		);
	Ins_ctrl_7seg: Ctrl_7seg port map(
		Num1 => Num1,
		Num2 => Num2,
		Num3 => Num3,
		Num4 => Num4,
		rst => rst,
		clk => clock_400,
		display_out => display_1,
		Char_out => char_1
		);
	Ins_flicker: Generador_Parpadeo port map(
		Char_in => char_1,
		Set_blink => Set_flicker,
		clk => clock_20,
		Char_out => char_out_temp
		);
	
	Char_out <= not char_out_temp;
	display_out <= display_1 & not (not Char_1(3) and Char_1(2) and not Char_1(1) and not Char_1(0)) ;

end dataflow;
