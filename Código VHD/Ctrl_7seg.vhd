library IEEE;
use IEEE.std_logic_1164.all;  -- Para std_logic
use IEEE.numeric_std.all;     -- Para unsigned

entity Ctrl_7seg is
   port( Num1: IN std_logic_vector (3 downto 0);
	 Num2: IN std_logic_vector (3 downto 0);
	 Num3: IN std_logic_vector (3 downto 0);
	 Num4: IN std_logic_vector (3 downto 0);
	 Clk: IN std_logic;
	 Rst: IN std_logic;
	 display_out: OUT std_logic_vector (6 downto 0);
	 Char_out: OUT std_logic_vector (3 downto 0) );
end Ctrl_7seg;

architecture dataflow of Ctrl_7seg is

	COMPONENT Decodificador
	Port(
		code : in  STD_LOGIC_VECTOR (3 downto 0);
		led:out std_logic_vector (6 downto 0)
		);
	End COMPONENT;

	COMPONENT Mux_4inputs
	Port(
		 Num1: IN std_logic_vector (3 downto 0);
		 Num2: IN std_logic_vector (3 downto 0);
		 Num3: IN std_logic_vector (3 downto 0);
		 Num4: IN std_logic_vector (3 downto 0);
		 Ctrl: IN std_logic_vector (3 downto 0);
		 Num_out: OUT std_logic_vector (3 downto 0)
		);
	End COMPONENT;

	COMPONENT ContadorAro
	Port(
		 DAT : out std_logic_vector(3 downto 0);
      		 RST : in std_logic;
       		 CLK : in std_logic
		);
	End COMPONENT;

  -- se�ales temporales
	signal Sel_Char : std_logic_vector (3 downto 0);
	signal Num_Activo : std_logic_vector (3 downto 0);

begin

	Ins_Contador: ContadorAro port map(
		DAT => Sel_Char,
		CLK => Clk,
		RST => Rst
		);
	Ins_Mux: Mux_4inputs port map(
		Num1 => Num1,
		Num2 => Num2,
		Num3 => Num3,
		Num4 => Num4,
		Ctrl => Sel_Char,
		Num_out => Num_Activo
		);
	Ins_Decod: Decodificador port map(
		code=> Num_Activo,
		led => display_out
		);
	Char_out <= Sel_Char;

end dataflow;
