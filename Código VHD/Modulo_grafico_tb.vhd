library IEEE;
use IEEE.std_logic_1164.all;  -- Para std_logic
use IEEE.numeric_std.all;     -- Para signed, unsigned

entity Modulo_grafico_tb is
end    Modulo_grafico_tb;

architecture behavioral of Modulo_grafico_tb is

   -- Component Declaration for the Unit Under Test (UUT)
   component Modulo_grafico
      port( 
	 Num1: IN std_logic_vector (3 downto 0);
	 Num2: IN std_logic_vector (3 downto 0);
	 Num3: IN std_logic_vector (3 downto 0);
	 Num4: IN std_logic_vector (3 downto 0);
	 Set_flicker: IN std_logic_vector (3 downto 0);
	 Clk: IN std_logic;
	 Rst: IN std_logic;
	 display_out: OUT std_logic_vector (7 downto 0);
	 Char_out: OUT std_logic_vector (3 downto 0) );
   end component;

   --Inputs
   signal Num1: std_logic_vector (3 downto 0):= "0001";
   signal Num2: std_logic_vector (3 downto 0):= "1001";
   signal Num3: std_logic_vector (3 downto 0):= "1000";
   signal Num4: std_logic_vector (3 downto 0):= "0011";
   signal Set_flicker: std_logic_vector (3 downto 0):= "1010";
   signal RST : std_logic := '0';
   signal CLK : std_logic := '0';
   --Outputs
   signal Char_out : std_logic_vector(3 downto 0);
   signal display_out : std_logic_vector(7 downto 0);
   -- Definicion del periodo del reloj
   constant CLK_period : time := 20 ns;

begin

   -- Instantiate the Unit Under Test (UUT)
   uut: modulo_grafico port map ( 
	Num1 => Num1,
	Num2 => Num2,
	Num3 => Num3,
	Num4 => Num4,
	Set_flicker => Set_flicker,
	rst => RST,
	clk => CLK,
	Char_out => Char_out,
	display_out => display_out);

   CLK_process :process
   begin
        CLK <= '0';
        wait for CLK_period/2;
        CLK <= '1';
        wait for CLK_period/2;
   end process;

   RST_process: process
   begin              
      RST <= '1';
      wait for 10 ns;  
      RST <= '0';
      wait;
   end process;

end behavioral;
