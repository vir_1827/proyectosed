library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Prueba_Cuenta_atras is
    Port ( BOTON1 : in  STD_LOGIC;
           BOTON2 : in  STD_LOGIC;
           clock : in  STD_LOGIC;
	   reset : in std_logic;
           digito_out : out  STD_LOGIC_VECTOR(3 downto 0);
           display_out : out  STD_LOGIC_VECTOR (7 downto 0)
	   );
end Prueba_Cuenta_atras;

architecture Behavioral of Prueba_Cuenta_atras is

signal clk: std_logic;

signal mm1_in: std_logic_vector(3 downto 0);
signal mm0_in: std_logic_vector(3 downto 0);
signal ss1_in: std_logic_vector(2 downto 0);
signal ss0_in: std_logic_vector(3 downto 0);

signal mm1: std_logic_vector(3 downto 0);
signal mm0: std_logic_vector(3 downto 0);
signal ss1: std_logic_vector(2 downto 0);
signal ss1_fix: std_logic_vector(3 downto 0);
signal ss0: std_logic_vector(3 downto 0);
signal char: std_logic_vector(3 downto 0);

component modulo_grafico
	port(	Num1: IN std_logic_vector (3 downto 0);
		Num2: IN std_logic_vector (3 downto 0);
		Num3: IN std_logic_vector (3 downto 0);
		Num4: IN std_logic_vector (3 downto 0);
		Set_flicker: IN std_logic_vector (3 downto 0);
		Clk: IN std_logic;
		Rst: IN std_logic;
		display_out: OUT std_logic_vector (7 downto 0);
		Char_out: OUT std_logic_vector (3 downto 0)
		);
end component;
component cuenta_atras
	Port ( 	clk  : IN  STD_LOGIC; --Reloj.
	        reset: IN  STD_LOGIC; --Reset.	
		pausa: IN  STD_LOGIC; --Pausa.
		M1_in   : IN STD_LOGIC_VECTOR(3 DOWNTO 0); --Primer digito de los minutos.
    		M0_in   : IN STD_LOGIC_VECTOR(3 DOWNTO 0); --Segundo digito de los minutos.
    	    	S1_in   : IN STD_LOGIC_VECTOR(2 DOWNTO 0); --Primer digito de los segundos.
        	S0_in   : IN STD_LOGIC_VECTOR(3 DOWNTO 0);  --Segundo digito de los segundos.
        	M1_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --Primer digito de los minutos.
        	M0_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --Segundo digito de los minutos.
        	S1_out   : OUT STD_LOGIC_VECTOR(2 DOWNTO 0); --Primer digito de los segundos.
        	S0_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);  --Segundo digito de los segundos.
		digito_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0) --Seleccion parpadeo
		);
end component;
component clk1Hz
	Port (
        	entrada: in  STD_LOGIC;
        	reset  : in  STD_LOGIC;
        	salida : out STD_LOGIC
    		);
end component;

begin
	ss1_fix <= '0'& ss1;

	Ins_clk1Hz: clk1Hz port map(
		entrada => clock,
		reset => reset,
		salida => clk
		);		
	Ins_modulo: modulo_grafico port map(
		Num1 => mm1,
		Num2 => mm0,
		Num3 => ss1_fix ,
		Num4 => ss0,
		Set_flicker => char,
		Rst => reset,
		Clk => clock,
		Char_out => digito_out,
		display_out => display_out
		);
	Ins_cuenta: cuenta_atras port map (
		clk  => clk,
	        reset  => BOTON1,	
		pausa  => BOTON2,
		M1_in  => mm1_in,
    		M0_in  => mm0_in,
    	    	S1_in  => ss1_in,
        	S0_in  => ss0_in,
        	M1_out  => mm1,
        	M0_out  => mm0,
        	S1_out  => ss1,
        	S0_out  => ss0,
		digito_out  => char
		);
end Behavioral;

