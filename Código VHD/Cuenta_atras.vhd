library IEEE;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_1164.ALL;
 
entity Cuenta_atras is
    PORT (
        clk  : IN  STD_LOGIC; --Reloj.
        reset: IN  STD_LOGIC; --Reset.	
	pausa: IN  STD_LOGIC; --Pausa.
	M1_in   : IN STD_LOGIC_VECTOR(3 DOWNTO 0); --Primer digito de los minutos.
        M0_in   : IN STD_LOGIC_VECTOR(3 DOWNTO 0); --Segundo digito de los minutos.
        S1_in   : IN STD_LOGIC_VECTOR(2 DOWNTO 0); --Primer digito de los segundos.
        S0_in   : IN STD_LOGIC_VECTOR(3 DOWNTO 0);  --Segundo digito de los segundos.
        M1_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --Primer digito de los minutos.
        M0_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --Segundo digito de los minutos.
        S1_out   : OUT STD_LOGIC_VECTOR(2 DOWNTO 0); --Primer digito de los segundos.
        S0_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);  --Segundo digito de los segundos.
	digito_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0) --Seleccion parpadeo
		  
    );
end Cuenta_atras;
 
architecture Behavioral of Cuenta_atras is
   	signal ss_1: UNSIGNED(2 downto 0) := "000" ;
    	signal ss_0: UNSIGNED(3 downto 0) := "0000";
   	signal mm_1: UNSIGNED(3 downto 0) := "0000" ;
    	signal mm_0: UNSIGNED(3 downto 0) := "0000";
	signal pause: std_logic:='1';
	signal temporal : std_logic_vector (3 downto 0):= "1111";
	signal pausa_anterior : std_logic := '1';
begin
	reloj: process (clk, reset, pausa)
	begin
	if reset = '1' then
		mm_1 <= UNSIGNED(M1_in);
		mm_0 <= UNSIGNED(M0_in);
		ss_1 <= UNSIGNED(S1_in);
		ss_0 <= UNSIGNED(S0_in);
		pause <= '1';
		
	elsif rising_edge(clk) then
		if (pausa /= pausa_anterior AND pausa ='1') then
			pause <= not pause;
		end if;	
		pausa_anterior <= pausa;
			
		if pause = '1' then
			mm_1 <= mm_1;
			mm_0 <= mm_0;
			ss_1 <= ss_1;
			ss_0 <= ss_0;

		elsif (ss_0 = 0) then

			if (ss_1 = 0) then
				if (mm_0 = 0) then
					if (mm_1 = 0) then
						pause <= '1';
					else 
						mm_1 <= mm_1-1;
						mm_0 <= "1001";
						ss_1 <= "101";
						ss_0 <= "1001";
					end if;
				else
					mm_0 <= mm_0-1;
					ss_1 <= "101";
					ss_0 <= "1001";
				end if;
			else
				ss_1 <= ss_1-1;
				ss_0 <= "1001";
			end if;
		else
			ss_0 <= ss_0-1;
		end if;
	end if;
end process;	

parpadeo: process (pause)
begin
	if (pause = '1') then
		temporal <= "1111";
	else
		temporal <= "0000";
	end if;
end process;
    --Asignaci�n de se�ales.
   	M1_out <= STD_LOGIC_VECTOR(mm_1);
   	M0_out <= STD_LOGIC_VECTOR(mm_0);
    	S1_out <= STD_LOGIC_VECTOR(ss_1);
   	S0_out <= STD_LOGIC_VECTOR(ss_0);
	digito_out <= temporal;
end Behavioral;