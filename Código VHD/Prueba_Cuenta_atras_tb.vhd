library IEEE;
use IEEE.std_logic_1164.all;  -- Para std_logic
use IEEE.numeric_std.all;     -- Para signed, unsigned

entity Prueba_Cuenta_atras_tb is
end    Prueba_Cuenta_atras_tb;

architecture behavioral of Prueba_Cuenta_atras_tb is

   -- Component Declaration for the Unit Under Test (UUT)
   component Prueba_Cuenta_atras
    	Port ( BOTON1 : in  STD_LOGIC;
      	     BOTON2 : in  STD_LOGIC;
      	     clock : in  STD_LOGIC;
	     reset : in std_logic;
     	     digito_out : out  STD_LOGIC_VECTOR(3 downto 0);
             display_out : out  STD_LOGIC_VECTOR (7 downto 0)
	     );
   end component;

   --Inputs
   signal RST : std_logic := '0';
   signal CLK : std_logic := '0';
   signal BOTON1 : std_logic := '0'; --Reset del contador
   signal BOTON2 : std_logic := '0'; -- Pausa del contador
   --Outputs
   signal digito_out : std_logic_vector(3 downto 0);
   signal display_out : std_logic_vector(7 downto 0);
   -- Definicion del periodo del reloj
   constant CLK_period : time := 20 ns;

begin

   -- Instantiate the Unit Under Test (UUT)
   uut: Prueba_Cuenta_atras port map ( 
	BOTON1 => BOTON1,
      	BOTON2 => BOTON2,
      	clock => CLK,
	reset => RST,
     	digito_out => digito_out,
        display_out => display_out
        );

   CLK_process :process
   begin
        CLK <= '0';
        wait for CLK_period/2;
        CLK <= '1';
        wait for CLK_period/2;
   end process;

   main_process: process
   begin        
      RST <= '1';
      wait for 10 ns;  
      RST <= '0';
--reseteamos el contador      
      wait for 50 ns;       
      BOTON1 <= '1';
      wait for 10 ns;  
      BOTON1 <= '0';
      wait for 50 ns;   
--Damos al boton de pause para poner en marcha el contador    
      BOTON2 <= '1';
      wait for 10 ns;  
      BOTON2 <= '0';
      wait;
   end process;

end behavioral;
