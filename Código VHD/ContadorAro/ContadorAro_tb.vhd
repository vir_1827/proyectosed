library IEEE;
use IEEE.std_logic_1164.all;  -- Para std_logic
use IEEE.numeric_std.all;     -- Para signed, unsigned

entity ContadorAro_tb is
end    ContadorAro_tb;

architecture Behavioral of ContadorAro_tb is
   --Inputs
   signal RST : std_logic := '0';
   signal CLK : std_logic := '0';
    --Outputs
   signal DAT : std_logic_vector(3 downto 0);
   -- Definicion del periodo del reloj
   constant CLK_period : time := 2 ns;

BEGIN

   uut: entity work.ContadorAro PORT MAP (
          DAT => DAT,
          RST => RST,
          CLK => CLK
        );

   CLK_process :process
   begin
        CLK <= '0';
        wait for CLK_period/2;
        CLK <= '1';
        wait for CLK_period/2;
   end process;

   RST_process: process
   begin              
      RST <= '1';
      wait for 10 ns;  
      RST <= '0';
      wait;
   end process;

END;
