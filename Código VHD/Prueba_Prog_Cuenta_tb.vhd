library IEEE;
use IEEE.std_logic_1164.all;  -- Para std_logic
use IEEE.numeric_std.all;     -- Para signed, unsigned

entity Prueba_Prog_Cuenta_tb is
end    Prueba_Prog_Cuenta_tb;

architecture Behavioral of Prueba_Prog_Cuenta_tb is
   --Inputs
        signal RST : std_logic := '0';
   	signal CLK : std_logic := '0';
	signal menos:  STD_LOGIC; --Pulsador restar 1 al digito seleccionado.
	signal mas:  STD_LOGIC; --Pulsador sumar 1 al digito seleccionado.
	signal Digito:  STD_LOGIC; --Seleccionar siguiente digito.
    --Outputs
        signal display_out   :  STD_LOGIC_VECTOR(7 DOWNTO 0);  
	signal digito_out :  STD_LOGIC_VECTOR(3 DOWNTO 0); 
    -- Definicion del periodo del reloj
    constant CLK_period : time := 20 ns;

BEGIN

   uut: entity work.Prueba_Prog_Cuenta PORT MAP (
	BOTON1 => menos,
	BOTON2 => mas,
	BOTON3 => digito,
	clock => CLK,
	reset => RST,
	digito_out => digito_out,
	display_out => display_out
        );
   
   CLK_process :process
   begin
        CLK <= '0';
        wait for CLK_period/2;
        CLK <= '1';
        wait for CLK_period/2;
   end process;

   main_process: process
   begin   
-- puesto todo a cero
	digito <='0';
	mas <= '0';
	menos <= '0';

     	RST <= '1';
      	wait for 10 ns;  
      	RST <= '0';   
	wait for 10 ns;

--restamos dos minutos
	menos <= '1';
	wait for 10 ns;
	menos <= '0';
 	wait for 20 ns;
	menos <= '1';
	wait for 10 ns;
	menos <= '0';
 	wait for 20 ns;

--cambiamos el digito y pasamos a editar minutos de 10 en 10
	digito <= '1';
	wait for 10 ns;
	digito <= '0';
 	wait for 20 ns;
--restamos 10 minutos
	menos <= '1';
	wait for 10 ns;
	menos <= '0';
 	wait for 20 ns;
--sumamos 10 minutos
	mas <= '1';
	wait for 10 ns;
	mas <= '0';
 	wait for 20 ns;

--cambiamos el digito y pasamos a editar horas de 1 en 1
	digito <= '1';
	wait for 10 ns;
	digito <= '0';
 	wait for 20 ns;
--sumamos 1 hora 
	mas <= '1';
	wait for 10 ns;
	mas <= '0';
 	wait for 20 ns;
--cambiamos  digito y pasamos a editar de 10 en 10 horas
	digito <= '1';
	wait for 10 ns;
	digito <= '0';
 	wait for 20 ns;
--sumamos 10 horas
	mas <= '1';
	wait for 10 ns;
	mas <= '0';
 	wait for 20 ns;
--restamos 10 horas
	menos <= '1';
	wait for 10 ns;
	menos <= '0';
 	wait for 20 ns;

   end process;

END;
