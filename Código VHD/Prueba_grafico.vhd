library IEEE;
use IEEE.std_logic_1164.all;  -- Para std_logic
use IEEE.numeric_std.all;     -- Para unsigned

entity Prueba_grafico is
   port( 
	 Set_flicker: IN std_logic_vector (3 downto 0);
	 Clk: IN std_logic;
	 Rst: IN std_logic;
	 display_out: OUT std_logic_vector (7 downto 0);
	 Char_out: OUT std_logic_vector (3 downto 0) );
end Prueba_grafico;

architecture dataflow of Prueba_grafico is

-- compoentes
component Modulo_grafico
   port( Num1: IN std_logic_vector (3 downto 0);
	 Num2: IN std_logic_vector (3 downto 0);
	 Num3: IN std_logic_vector (3 downto 0);
	 Num4: IN std_logic_vector (3 downto 0);
	 Set_flicker: IN std_logic_vector (3 downto 0);
	 Clk: IN std_logic;
	 Rst: IN std_logic;
	 display_out: OUT std_logic_vector (7 downto 0);
	 Char_out: OUT std_logic_vector (3 downto 0) );
end component;

-- se�ales temporarles
signal Num1: std_logic_vector (3 downto 0):= "0001";
signal Num2: std_logic_vector (3 downto 0):= "1001";
signal Num3: std_logic_vector (3 downto 0):= "1000";
signal Num4: std_logic_vector (3 downto 0):= "0011";

begin

	Ins_Modulo_grafico: Modulo_grafico port map(
		Num1 => Num1,
		Num2 => Num2,
		Num3 => Num3,
		Num4 => Num4,
		Set_flicker => Set_flicker,
		rst => rst,
		clk => clk,
		display_out => display_out,
		Char_out => char_out
		);

end dataflow;
