library IEEE;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_1164.ALL;

entity program_cuenta is
  
  PORT (     
	Control: IN  STD_LOGIC; --Se�al de activaci�n.
	menos: IN  STD_LOGIC; --Pulsador restar 1 al digito seleccionado.
	mas: IN  STD_LOGIC; --Pulsador sumar 1 al digito seleccionado.
	Digito: IN  STD_LOGIC; --Seleccionar siguiente digito.
	M1_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --SALIDA Segundo digito de la hora.    
	M0_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --SALIDA Primer digito de la hora.     
	S1_out   : OUT STD_LOGIC_VECTOR(2 DOWNTO 0); --SALIDA Segundo digito de los minutos.    
	S0_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);  --SALIDA Primer digito de los minutos.
	digito_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0) --Codigo del digito que se esta editando.
	);
end program_cuenta;

architecture Behavioral of program_cuenta is
   
signal ss1: UNSIGNED(2 downto 0) := "000" ;   
signal ss0: UNSIGNED(3 downto 0) := "0000";   
signal mm1: UNSIGNED(3 downto 0) := "0000" ;  
signal mm0: UNSIGNED(3 downto 0) := "0000";   
signal DIG: STD_LOGIC_VECTOR (3 downto 0):= "0001";
signal digito_anterior: STD_LOGIC;
signal menos_anterior: STD_LOGIC;
signal mas_anterior: STD_LOGIC;

begin

principal: process (control, mas, menos, digito)

begin
        
	if control = '0' then     
	DIG <= "0001";

	elsif (digito /= digito_anterior and digito = '1') then
		DIG <= to_stdlogicvector(to_bitvector(DIG) rol 1);

   elsif (menos /= menos_anterior and menos = '1') then

		if DIG = "0001" then --Se edita el primer digito de los minutos
			ss0 <= ss0 - 1;
			if ss0 = 0 then
				ss0 <= "1001";
			end if;
		elsif DIG = "0010" then --Se edita el segundo digito de los minutos
			ss1 <= ss1 - 1;  
			if ss1 = 0 then
				ss1 <= "101";
			end if;
		elsif DIG = "0100" then --Se edita el primer digito de las horas
			mm0 <= mm0 - 1;
			if mm0 = 0 then
				mm0 <= "1001";
			end if;
		elsif DIG = "1000" then --Se edita el segundo digito de las horas
			mm1 <= mm1 - 1;
			if mm1 = 0 then
				mm1 <= "1001";
			end if;
		end if;

	elsif (mas /= mas_anterior and mas = '1') then

		if DIG = "0001" then --Se edita el primer digito de los minutos
			ss0 <= ss0 + 1;
			if ss0 = 9 then
				ss0 <= "0000";
			end if;
		elsif DIG = "0010" then --Se edita el segundo digito de los minutos
			ss1 <= ss1 + 1;
			if ss1 = 5 then
				ss1 <= "000";
			end if;
	   elsif DIG = "0100" then --Se edita el primer digito de las horas
			mm0 <= mm0 + 1;
			if mm0 = 9 then
				mm0 <= "0000";
			end if;
	   elsif DIG = "1000" then --Se edita el segundo digito de las horas
			mm1 <= mm1 + 1;
			if mm1 = 9 then
				mm1 <= "0000";
			end if;
	   end if;
	end if;
	digito_anterior <= digito;
	menos_anterior <= menos;
	mas_anterior <= mas;
	
end process;

--Asignaci�n de se�ales.

    M1_out <= STD_LOGIC_VECTOR(mm1);
    M0_out <= STD_LOGIC_VECTOR(mm0);
    S1_out <= STD_LOGIC_VECTOR(ss1);
    S0_out <= STD_LOGIC_VECTOR(ss0);

    digito_out <= DIG;

end Behavioral;



