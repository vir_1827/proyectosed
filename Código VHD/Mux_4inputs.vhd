library IEEE;
use IEEE.std_logic_1164.all;  -- Para std_logic
use IEEE.numeric_std.all;     -- Para unsigned

entity Mux_4inputs is
   port( Num1: IN std_logic_vector (3 downto 0);
	 Num2: IN std_logic_vector (3 downto 0);
	 Num3: IN std_logic_vector (3 downto 0);
	 Num4: IN std_logic_vector (3 downto 0);
	 Ctrl: IN std_logic_vector (3 downto 0);
	 Num_out: OUT std_logic_vector (3 downto 0) );
end Mux_4inputs;

architecture behavioral of Mux_4inputs is
begin
with Ctrl select Num_out <=
	Num1 when "1000",
	Num2 when "0100",
	Num3 when "0010",
	Num4 when "0001",
	"1111" when others;
end behavioral;
