library IEEE;
use IEEE.std_logic_1164.all;  -- Para std_logic
use IEEE.numeric_std.all;     -- Para unsigned

entity Generador_Parpadeo is
   port( Num_in: IN STD_LOGIC_VECTOR (6 downto 0);
	 Char_in: IN STD_LOGIC_VECTOR (3 downto 0);
	 Set_flicker: IN STD_LOGIC_VECTOR (3 downto 0);
	 clk: IN STD_LOGIC;
	 Num_out: OUT STD_LOGIC_VECTOR (6 downto 0);
	 Char_out: OUT STD_LOGIC_VECTOR (3 downto 0));
end Generador_Parpadeo;

architecture Behavioral of Generador_Parpadeo is
SIGNAL temp: STD_LOGIC_VECTOR (6 downto 0):="0000000";
begin

process (Char_in, Num_in, Set_flicker, Clk)
begin
	if ((Set_flicker(0)='1' AND Char_in(0)='1') OR 
	    (Set_flicker(1)='1' AND Char_in(1)='1') OR
    	    (Set_flicker(2)='1' AND Char_in(2)='1') OR 
	    (Set_flicker(3)='1' AND Char_in(3)='1')) then
		if (clk = '1') then
			temp <= "1111111";
		else
			temp <= Num_in;
		end if;
	else
		temp <= Num_in;
	end if;
end process;

Char_out <= Char_in;
Num_out <= temp;

end behavioral;
