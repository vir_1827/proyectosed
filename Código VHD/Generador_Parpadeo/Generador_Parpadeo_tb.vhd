library IEEE;
use IEEE.std_logic_1164.all;  -- Para std_logic
use IEEE.numeric_std.all;     -- Para signed, unsigned

entity Generador_Parpadeo_tb is
end    Generador_Parpadeo_tb;

architecture Behavioral of Generador_Parpadeo_tb is
   --Inputs
	signal Num_in: STD_LOGIC_VECTOR (6 downto 0):="0001111";
	signal Char_in: STD_LOGIC_VECTOR (3 downto 0);
	signal Set_flicker: STD_LOGIC_VECTOR (3 downto 0):="0101";
	signal clk: STD_LOGIC;
    --Outputs
	signal Num_out: STD_LOGIC_VECTOR (6 downto 0);
	signal Char_out: STD_LOGIC_VECTOR (3 downto 0);
   -- Definicion del periodo del reloj
   constant CLK_period : time := 500 ns;

BEGIN

   uut: entity work.Generador_Parpadeo PORT MAP (
	Num_in => Num_in,
	Char_in => Char_in,
	Set_flicker => Set_flicker,
	clk => clk,
	Num_out => Num_out,
	Char_out => Char_out
        );

   CLK_process :process
   begin
        CLK <= '0';
        wait for CLK_period/2;
        CLK <= '1';
        wait for CLK_period/2;
   end process;

   main_process: process
   begin      
	Char_in <= "0100"  ;
	wait for 100 ns;    
 	Char_in <= "0010" ;
	wait for 100 ns;    
 	Char_in <= "0001" ;
	wait for 100 ns;    
 	Char_in <= "1000" ;
	wait for 100 ns; 
   end process;

END;
