--------------------------------------------------------------------------------
-- Modulo para quitar los rebotes de un pulsador.
-- El pulsador se conecta a la se�al de entrada "button".
-- La se�al "salida" se pone a uno cuando el bot�n esta pulsado   
-- y sin rebotes durante 3 ciclos de reloj "clk"
-- La se�al de reloj "clk" debe ser de 100Hz aproximadamente para obtener
-- un buen antirrebotes.
-- Mientras m�s bits tenga el registro "shift",
-- mayor ser� la protecci�n antirebotes

--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all; 

entity antirebotes is
  port (
     clk    : in  std_logic;
     rst    : in  std_logic;
     button : in  std_logic;
     salida : out std_logic
   );
end entity antirebotes;

architecture behavioral of antirebotes is

signal shift_reg : std_logic_vector(2 downto 0);

begin    -- architecture behavioral

SHIFT_REG_PROC : process(clk, rst) is
  begin
    if (rst = '1') then
     shift_reg <= "000";
   elsif (clk'event and clk = '1')   then
     shift_reg <= button & shift_reg(2 downto 1);
   end if;
  end process SHIFT_REG_PROC;

salida <= shift_reg(2) and shift_reg(1) and shift_reg(0);

end architecture behavioral;
