library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Prueba_Cuenta_Completa is
    Port ( BOTON1 : in  STD_LOGIC;
           BOTON2 : in  STD_LOGIC;
           BOTON3 : in  STD_LOGIC;
           BOTON4 : in  STD_LOGIC;
           clock : in  STD_LOGIC;
			  reset : in std_logic;
           digito_out : out  STD_LOGIC_VECTOR(3 downto 0);
           display_out : out  STD_LOGIC_VECTOR (7 downto 0)
			  );
end Prueba_Cuenta_Completa;

architecture Behavioral of Prueba_Cuenta_Completa is

signal mm1: std_logic_vector(3 downto 0);
signal mm0: std_logic_vector(3 downto 0);
signal ss1: std_logic_vector(2 downto 0);
signal ss1_fix: std_logic_vector(3 downto 0);
signal ss0: std_logic_vector(3 downto 0);
signal char: std_logic_vector(3 downto 0);

component modulo_grafico
	port( Num1: IN std_logic_vector (3 downto 0);
			Num2: IN std_logic_vector (3 downto 0);
			Num3: IN std_logic_vector (3 downto 0);
			Num4: IN std_logic_vector (3 downto 0);
			Set_flicker: IN std_logic_vector (3 downto 0);
			Clk: IN std_logic;
			Rst: IN std_logic;
			display_out: OUT std_logic_vector (7 downto 0);
			Char_out: OUT std_logic_vector (3 downto 0)
			);
end component;
component Cuenta_atras_completa
	Port ( BOTON1 : in  STD_LOGIC; -- Reset del contador // Restar 1 en programacion
          BOTON2 : in  STD_LOGIC; -- Pausa del contador // Sumar 1 en programacion
          BOTON3 : in  STD_LOGIC; -- Cambio de digito en programacion
			BOTON4 : in STD_LOGIC; -- Paso de programacion a contador
        CLK : in  STD_LOGIC;
		  digito_out : out std_logic_vector (3 downto 0);
		  M1_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
		  M0_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); 
		  S1_out   : OUT STD_LOGIC_VECTOR(2 DOWNTO 0); 
		  S0_out   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0) 
	);
end component;
begin
		ss1_fix <= '0'& ss1;
		
		Ins_modulo: modulo_grafico port map(
			Num1 => mm1,
			Num2 => mm0,
			Num3 => ss1_fix ,
			Num4 => ss0,
			Set_flicker => char,
			Rst => reset,
			Clk => clock,
			Char_out => digito_out,
			display_out => display_out
			);
	Ins_cuenta: Cuenta_atras_completa port map (
			BOTON1 => BOTON1,
			BOTON2 => BOTON2,
			BOTON3 => BOTON3,
			BOTON4 => BOTON4,
			CLK => clock,
			M1_out => mm1,
			M0_out => mm0,
			S1_out => ss1,
			S0_out => ss0,
			digito_out => char
			);
end Behavioral;

